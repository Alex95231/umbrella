import { App } from './ts/app';
import { CanvasWorkPlace } from './ts/canvasWorkPlace';
import './styles/styles.css';
import './styles/reset.css'; 
import './styles/viewer.css'; 
import './styles/global.css'; 
import './styles/preloader.css'; 
import './styles/pop-up-link-info.css';
import "./styles/customSelect.css";

import { fabric } from "fabric";

const app = new App();


// const canvasWorkPlace = new CanvasWorkPlace();