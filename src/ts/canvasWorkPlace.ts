import {} from "three";

import Helpers from "./Helpers/helpers";

import { fabric } from "fabric";
import axios from "axios";


export class CanvasWorkPlace {
  config:any;

  
  constructor(config:any) {
    const canvas = new fabric.Canvas("canvas");
    canvas.selection = false;
    canvas.setHeight(1200);
    canvas.setWidth(1200);

    const helpers = new Helpers(canvas);

    this.uploadFiles(canvas, "0");
    this.onClickBody(canvas);
    this.config = config;

    if(config.config){
      config.config.configImage && this.uploadFilesFromConfi(canvas, this.config);
    }

    const temp = document.getElementById("ul-list-files");
    temp.addEventListener("click", (e:any) => {    
     
        
        e.target.classList[0] === "wand"  && this.removeBackgroundColor(canvas, e.target);
        e.target.classList[0] === "remove-file" && this.removeImageFromCanvas(canvas, e.target);
        e.target.classList[0] === "copy-file" && this.copyFile(canvas, e.target);

    });
  }


  uploadFilesFromConfi(canvas: any, config:any) {
        // ----- CREATE NEW FILE
          const ul: HTMLInputElement = <HTMLInputElement>(
            document.getElementById("ul-list-files")
          );
          const list = document.getElementById("files-list");
          list.append(ul);
  
         for(let i = 0; i < config.config.configImage.length; i++){
            const id = `${1 - 0.5 + Math.random() * (1000000 - 1 + 1)}`;
  
            const name: String = config.config.configImage[i].imagepath.split("/")[config.config.configImage[i].imagepath.split("/").length-1];
            const div: any = document.createElement("div");
            div.id = `${id}`;
  
            //create icon
            const iMagic: any = document.createElement("div");
            // iMagic.classList.add("material-icons");
            iMagic.classList.add("wand");
            // iMagic.textContent = "star_border";
  
            const iCopy: any = document.createElement("div");
            // iCopy.classList.add("material-icons");
            iCopy.classList.add("copy-file");
            // iCopy.textContent = "content_copy";
  
            const iDelete: any = document.createElement("div");
            // iDelete.classList.add("material-icons");
            iDelete.classList.add("remove-file");
            // iDelete.textContent = "clear";
  
            const iDeleteWrapper: any = document.createElement("div");
            iDeleteWrapper.classList.add("delete-image");
            iDeleteWrapper.append(iDelete);
  
            const li: any = document.createElement("li");
            li.textContent = name;
            li.classList.add("item_list_files");
            div.append(li);
            div.append(iMagic);
            div.append(iCopy);
            div.append(iDeleteWrapper);
            ul.append(div);

            fetch(config.config.configImage[i].imagepath)
              .then(res => res.blob())
              .then(blob => {
                  let file = new File([blob], `0`,{ type: "image/png"});
                  this.uploadFilesToCanvas(canvas, file, id, config.config.configImage[i]);
              })
  
            document
              .querySelector(".canvas-container")
              .addEventListener("click", (e) => {
                this.onClickImage(canvas, id);
              });
            document
              .querySelector(".canvas-container")
              .removeEventListener("click", (e) => {
                this.onClickImage(canvas, id);
              });  
          }
  }

  //UPLOAD FILES
  uploadFiles(canvas: any, id: string): void {
    if (id === "0") {
      // ----- CREATE NEW FILE
      document.getElementById("image-file").onchange = () => {
        const x: HTMLInputElement = <HTMLInputElement>(
          document.getElementById("image-file")
        );
        const ul: HTMLInputElement = <HTMLInputElement>(
          document.getElementById("ul-list-files")
        );
        const list = document.getElementById("files-list");
        list.append(ul);

        for (let i: number = 0; i < x.files.length; i++) {
          const id = `${1 - 0.5 + Math.random() * (1000000 - 1 + 1)}`;

          const name: String = x.files[i].name.split(".")[0];
          const div: any = document.createElement("div");
          div.id = `${id}`;

          //create icon
          const iMagic: any = document.createElement("div");
          // iMagic.classList.add("material-icons");
          iMagic.classList.add("wand");
          // iMagic.textContent = "star_border";

          const iCopy: any = document.createElement("div");
          // iCopy.classList.add("material-icons");
          iCopy.classList.add("copy-file");
          // iCopy.textContent = "content_copy";

          const iDelete: any = document.createElement("div");
          // iDelete.classList.add("material-icons");
          iDelete.classList.add("remove-file");
          // iDelete.textContent = "clear";

          const iDeleteWrapper: any = document.createElement("div");
          iDeleteWrapper.classList.add("delete-image");
          iDeleteWrapper.append(iDelete);

          const li: any = document.createElement("li");
          li.textContent = name;
          li.classList.add("item_list_files");
          div.append(li);
          div.append(iMagic);
          div.append(iCopy);
          div.append(iDeleteWrapper);
          ul.append(div);

          this.uploadFilesToCanvas(canvas, x.files[i], id, false);

          document
            .querySelector(".canvas-container")
            .addEventListener("click", (e) => {
              this.onClickImage(canvas, id);
            });
          document
            .querySelector(".canvas-container")
            .removeEventListener("click", (e) => {
              this.onClickImage(canvas, id);
            });  
        }
      };
    } else {
      //   ------   COPY SELECTED FILE
      const tempCopyFileName: HTMLInputElement = <HTMLInputElement>(
        document.getElementById(id).childNodes[0]
      );
      const copyFileName: String = tempCopyFileName.innerHTML + "_copy";
      const idCopy: String = id + "_copy";

      const ul: HTMLUListElement = <HTMLUListElement>(
        document.getElementById("ul-list-files")
      );
      const div: any = document.createElement("div");
      div.id = `${idCopy}`;

      //create icon
      const iMagic: any = document.createElement("div");
      // iMagic.classList.add("material-icons");
      iMagic.classList.add("wand");
      // iMagic.textContent = "star_border";

      const iCopy: any = document.createElement("div");
      // iCopy.classList.add("material-icons");
      iCopy.classList.add("copy-file");
      // iCopy.textContent = "content_copy";

      const iDelete: any = document.createElement("div");
      // iDelete.classList.add("material-icons");
      iDelete.classList.add("remove-file");
      // iDelete.textContent = "clear";

      const iDeleteWrapper: any = document.createElement("div");
      iDeleteWrapper.classList.add("delete-image");
      iDeleteWrapper.append(iDelete);

      const li: any = document.createElement("li");
      li.textContent = copyFileName;
      li.classList.add("item_list_files");
      div.append(li);
      div.append(iMagic);
      div.append(iCopy);
      div.append(iDeleteWrapper);
      ul.append(div);

      document
        .querySelector(".canvas-container")
        .addEventListener("click", (e) => {
          this.onClickImage(canvas, id);
        });

      document
        .querySelector(".canvas-container")
        .removeEventListener("click", (e) => {
          this.onClickImage(canvas, id);
        });
    }
  }

  //ADD IMAGE TO FABRIC
  addFileToFabricJS(canvas: any, file: any, id: string, config:any): void {
    const reader = new FileReader();
    reader.onload = (event: any) => {
      const imgObj: any = new Image();
      imgObj.src = event.target.result;
      imgObj.onload = () => {
        const image = new fabric.Image(imgObj);
        image.scale(0.1);
        canvas.centerObject(image);
        image.setControlsVisibility({
          tl:true, //top-left
          mt:false, // middle-top
          tr:true, //top-right
          ml:false, //middle-left
          mr:false, //middle-right
          bl:true, // bottom-left
          mb:false, //middle-bottom
          br:true //bottom-right
         })

         image.setCoords()

        config ?
         image.set({
          scaleX: config.scale.scaleX,
          scaleY: config.scale.scaleY,
          angle: config.angle,
          left: +config.position.left, 
          top: +config.position.top,
          borderColor: "red",
          cornerColor: "white",
          cornerStrokeColor: "black",
          cornerSize: document.body.clientWidth <= 700 ? 70 : 30,
          rotatingPointOffset: document.body.clientWidth <= 700 ? 500 : 1000,
          cornerStyle: "circle",
          transparentCorners: false,
          centeredScaling: true,
        }):
        image.set({
          scaleX: 0.5,
          scaleY: 0.5,
          borderColor: "red",
          cornerColor: "white",
          cornerStrokeColor: "black",
          cornerSize: document.body.clientWidth <= 700 ? 70 : 30,
          rotatingPointOffset: document.body.clientWidth <= 700 ? 500 : 1000,
          cornerStyle: "circle",
          transparentCorners: false,
          centeredScaling: true,
        });

        canvas.add(image);
        canvas.renderAll();
        image.cacheKey = id;
      };
    };
    reader.readAsDataURL(file);
  }


  //UPLOAD FILE TO CANVAS
  uploadFilesToCanvas(canvas: any, file: any, id: string, config:any): void {
    this.addFileToFabricJS(canvas, file, id, config);
  }

  //COPY FILE
  copyFile(canvas: any, event: any): void {
    let clipboard: any;
    if (event.classList[0] === "copy-file") {
      const id: string = event.parentNode.id;
      canvas.getObjects().forEach((o: any) => {
        if (o.cacheKey == id) {
          canvas.setActiveObject(o);
          canvas.renderAll();
        }
      });

      
      canvas.getActiveObject().clone((cloned: any) => {
        console.log(cloned);
        clipboard = cloned;
        clipboard.left -= 50;
        clipboard.top -= 50;
        clipboard.cacheKey = id + "_copy";
        clipboard.set({
          scaleX: cloned.scaleX,
          scaleY: cloned.scaleY,
          borderColor: "red",
          cornerColor: "white",
          cornerStrokeColor: "black",
          cornerSize: document.body.clientWidth <= 700 ? 70 : 30,
          rotatingPointOffset: document.body.clientWidth <= 700 ? 70 : 1000,
          cornerStyle: "circle",
          transparentCorners: false,
          centeredScaling: true,
        });
        canvas.add(clipboard);
        canvas.requestRenderAll();
      });

      this.uploadFiles(canvas, id);
    }
  }

  //REMOVE FILE FROM FABRIC JS
  removeImageFromCanvas(canvas: any, event: any): void {
    console.log(event.classList)
    if (event.classList[0] === "remove-file") {
      const id: string = event.parentNode.parentNode.id;
      const object = canvas.getObjects();
      object.map((item: any, i: number) => {
        if (item.cacheKey === id) {
          canvas.remove(object[i]);
          document.getElementById(id).remove();
        }
      });
    }
  }

  // SELECT FILEINTO LIST AFTER CLICKED ON FILE ONTO FABRIC
  onClickImage(canvas: any, id: string): void {
    if (canvas.getActiveObject()) {
      const idActive = canvas.getActiveObject().cacheKey;
      const activeFile = document.getElementById(idActive);
      activeFile.classList.add("active-file");

      const temp = canvas.getObjects();
      temp.forEach((element: any) => {
        if (element.cacheKey !== idActive) {
          document
            .getElementById(element.cacheKey)
            .classList.remove("active-file");
        }
      });
    } else {
      if (canvas.getObjects().length > 0) {
        const activeFile = document.getElementById(id);
        activeFile.classList.remove("active-file");
      }
    }
  }

  //DESELECT ACTIVE OBJECT ON FABRIC
  onClickBody(canvas: any): void {
    const bodyElement: HTMLBodyElement = <HTMLBodyElement>(
      document.querySelector("body")
    );
    bodyElement.addEventListener("click", (e: any) => {
      const classNameOfTarget = e.target.className;
      if (classNameOfTarget !== "upper-canvas ") {
        canvas.discardActiveObject();
        canvas.requestRenderAll();
      }
    });
  }

  removeBackgroundColor(canvas: any, event: any) {
    const uvCcontainer = document.getElementById('uv-preloader');
    uvCcontainer.classList.add('uv-preloader');
    const removeBgKEY = 'xXWdSHMhWCoeKDUoWwkPyXHq';
    if (event.classList[0] === "wand") {
      const object = canvas.getObjects();
      object.map((item: any, i: number) => {
        const id: string = event.parentNode.id;
        if (item.cacheKey === id) {
          const object = canvas.getObjects();
          object.map((item: any, i: number) => {
          if (item.cacheKey === id) {
            canvas.remove(object[i]);
          }
          });
          
          const url = item._element.currentSrc;
          fetch(url)
          .then(res => res.blob())
          .then(blob => {
            const file = new File([blob], "File name",{ type: "image/png" });
            const data = new FormData();
            
            data.append('image_file',file);
            data.append('size','auto');
            axios.post('https://api.remove.bg/v1.0/removebg',data,{
                headers: {
                  'X-Api-Key': removeBgKEY,
                  'X-Type': 'application/json',
                  'Accept': 'application/json'
                }
                }
              ).then(res=>{
            
                const imgObj: any = new Image();   
                imgObj.src = `data:image/jpeg;base64,${res.data.data.result_b64}`;
                imgObj.onload = () => {
                  const image = new fabric.Image(imgObj);
                  image.scale(0.1);
                  canvas.centerObject(image);
                  image.set({
                    scaleX: 0.5,
                    scaleY: 0.5,
                    borderColor: "red",
                    cornerColor: "white",
                    cornerStrokeColor: "black",
                    cornerSize: document.body.clientWidth <= 700 ? 70 : 30,
                    rotatingPointOffset: document.body.clientWidth <= 700 ? 5000 : 1000,
                    cornerStyle: "circle",
                    transparentCorners: false,
                    centeredScaling: true,
                  });
                  canvas.add(image);
                  canvas.renderAll();
                  image.cacheKey = item.cacheKey;
                  uvCcontainer.classList.remove('uv-preloader');
                }
              }).catch(rej=>console.log('error',rej))

            })  
        }
      });
    }
  }
}
