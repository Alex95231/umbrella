import * as English from '../../languagePackges/english.json';
import * as Deutsch from '../../languagePackges/deutsch.json';
//   import English from '../../languagePackges/english.json';

import axios from 'axios';
  
class Helpers {
    canvas: fabric.Canvas;


    constructor(canvas: fabric.Canvas) {
        this.canvas = canvas;
        const createLinkBtn: HTMLButtonElement = <HTMLButtonElement>(
            document.querySelector(".create-link")
        );
        createLinkBtn.addEventListener('click', ()=>{
            this.getAllObjects('create-link');
        })

        const secondCreateLinkBtn: HTMLButtonElement = <HTMLButtonElement>(
            document.querySelector(".create-request")
        );
        secondCreateLinkBtn.addEventListener('click', ()=>{
            this.getAllObjects('second-create-link');
        })

        // this.changeLanguage();
    }

    //FROM BASE64 TO BLOB
    dataURItoBlob(dataURI: any) {
        let byteString = atob(dataURI.split(',')[1]);
    
        let ab = new ArrayBuffer(byteString.length);
        let ia = new Uint8Array(ab);
        for (let i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ab]);
    }


    // seven button
    getAllObjects(type: string): void{
        const tempActiveColor: any =  document.querySelector('.st1');
        const activeColor = tempActiveColor.style.fill;

        const data:any = new FormData();

        const stringScripJSON: any = document.getElementsByTagName('script')[0].innerHTML; //change to [0]
        const readyScripJSON: any = JSON.parse(stringScripJSON.split('=')[1].split(';')[0].split("'")[1]);

        const testPath = `${readyScripJSON.page_url}#`;
        const temp = readyScripJSON.save_data_url;  //'https://schirmmacher.it/3d-designer-2021/dist/3d-design-app.php'
        const path: HTMLInputElement = <HTMLInputElement>(
            document.querySelector(".link-line")
        );
        const secondPath: HTMLInputElement = <HTMLInputElement>(
            document.querySelector(".link-line-second")
        );

        path.value="";
        secondPath.value="";
        const preloader: HTMLDivElement = <HTMLDivElement>(
            document.querySelector('.link-line-preloader')
        );
        preloader.classList.add('input-loading');
        secondPath.classList.add('input-loading');
    
        if(this.canvas.getObjects().length  > 0){
            const configImage:any = [];

            data.append('domain', readyScripJSON.domain);
            data.append('slug', readyScripJSON.slug);
            data.append('art_id', readyScripJSON.art_id);   

            this.canvas.getObjects().forEach((obj: any, indx:number) => {

                const url = obj._element.currentSrc;
    
                configImage.push({
                    "id":`${indx}`,//obj.aCoords
                    "aCoords": {
                        "tl":{"x":`${obj.aCoords.tl.x}`,"y":`${obj.aCoords.tl.y}`},
                        "tr":{"x":`${obj.aCoords.tr.x}`,"y":`${obj.aCoords.tr.y}`},
                        "bl":{"x":`${obj.aCoords.bl.x}`,"y":`${obj.aCoords.bl.y}`},
                        "br":{"x":`${obj.aCoords.br.x}`,"y":`${obj.aCoords.br.y}`}
                    },
                    "position": {
                        "top": `${obj.top}`,
                        "left": `${obj.left}`
                        },
                    "scale": {
                        "scaleX": `${obj.scaleX}`,
                        "scaleY": `${obj.scaleY}`
                    },
                    "angle": obj.angle ? `${obj.angle}` : "0"
                });

                fetch(url)
                    .then(res => res.blob())
                    .then(blob => {
                        let file = new File([blob], `${indx}`,{ type: "image/png"});

                        data.append('images[]', file);
                    
                        if(indx === this.canvas.getObjects().length-1){
                            data.append("config", `{"activColor": "${activeColor}", "configImage": ${JSON.stringify(configImage)} }`);
                            
                            axios.post(
                                temp, 
                                data
                                ) .then((response:any) => {
                                    if(type === 'create-link'){
                                        preloader.classList.remove('input-loading');
                                        path.value=testPath+`${response.data.hash}`;
                                    }
                                    else{
                                        secondPath.classList.remove('input-loading');
                                        secondPath.value=testPath+`${response.data.hash}`;
                                    }
                                  })
                                  .catch((response:any) => {
                                    console.log('response',response);
                                  }); 
                        }
                });
            
            });
        }
        else{
            data.append("domain", readyScripJSON.domain);
            data.append("slug", readyScripJSON.slug);
            data.append("art_id", readyScripJSON.art_id);   
            data.append("config", `{"activColor": "${activeColor}"}`);

            axios.post(temp, data) 
                .then((response:any) => {
                    if(type === 'create-link'){
                        preloader.classList.remove('input-loading');
                        path.value=testPath+`${response.data.hash}`;
                    }
                    else{
                        secondPath.classList.remove('input-loading');
                        secondPath.value=testPath+`${response.data.hash}`;
                    }
                })
                .catch((response:any) => {
                    console.log('response',response);
                });
        }
    
    }

    //CHANGE LANGUAGE
    changeLanguage():void {
        document.querySelector('.custom-select-wrapper').addEventListener('click', function() {
            this.querySelector('.custom-select').classList.toggle('open');
        })
        const listLng:  any = document.querySelectorAll(".custom-option");
        for (const option of listLng) {
            option.addEventListener('click', (e: any) => {
                if (!e.target.classList.contains('selected')) {
                    const language: String = e.target.getAttribute('data-value');
                    this.changeAllTextToSelectedLanguage(language);
                    e.target.parentNode.querySelector('.custom-option.selected').classList.remove('selected');
                    e.target.classList.add('selected');
                    e.target.closest('.custom-select').querySelector('.custom-select__trigger span').textContent = e.target.textContent;
                }
            })
        }

        window.addEventListener('click', function(e) {
            const select = document.querySelector('.custom-select')
            const target: any = e.target;
            if (!select.contains(target)) {
                select.classList.remove('open');
            }
        });
    }

    //CHANG LANGUAGE TEXT TO SELECTED LANGUAGE
    changeAllTextToSelectedLanguage(language: any): void {
        let selectedLanguage = English;
        switch(language) {
            case 'En':  
                selectedLanguage = English;
                break;
            case 'De':  
                selectedLanguage = Deutsch;
                break;
            default:
                selectedLanguage = Deutsch;
              break;
          }
        const arrayOfKey = Object.keys(selectedLanguage);
        arrayOfKey.forEach(key =>{
            if(typeof selectedLanguage[key] == 'object'){
                Object.keys(selectedLanguage[key]).forEach(keydep =>{
                    document.getElementById(keydep).innerHTML = selectedLanguage[key][keydep];
                })
            }
            else{
                document.getElementById(key).innerHTML = selectedLanguage[key];
            }
            
        })
    }
   
  }
  
  export default Helpers;