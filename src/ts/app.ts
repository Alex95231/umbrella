import {
  SphereGeometry,
  MeshBasicMaterial,
  Mesh,
  CircleGeometry,
  MeshStandardMaterial,
  MeshPhongMaterial,
} from "three";
import * as THREE from "three";
import Viewer from "./Viewer";
import Loader from "./loader";
import axios from "axios";
import { CanvasWorkPlace } from "./canvasWorkPlace";

// import umbrella from "../assets/models/Umbrella/umbrella.FBX";
import umbrella from "../assets/uvMap/umbrella_com/Umbrella_1.fbx";
import imageOctagon from "../assets/uvMap/umbrella_com/Umbrella_fabric_inscription.png";

export class App {
  viewer: Viewer;
  geometry: SphereGeometry;
  material: MeshBasicMaterial;
  sphere: Mesh;
  loader: Loader;
  canvasWorkPlace: any;

  constructor() {
    this.viewer = new Viewer();

    window.onload = () => {
      const config: any = this.getRequetForMainInitial();
      //LOADER
      this.loader = new Loader(this.viewer, umbrella, config);
      const canvasWorkPlace = new CanvasWorkPlace(config);
    };

    const planeSize: number = 400;

    //PLANE
    const planeGeo = new CircleGeometry(planeSize, planeSize);
    const planeMat = new THREE.MeshStandardMaterial({ color: 0xffffff });
    const plane = new Mesh(planeGeo, planeMat);
    plane.name = "Plane";
    plane.rotateX(Math.PI * -0.5);
    plane.receiveShadow = true;
    plane.position.y = -40;
    this.viewer.scene.add(plane);

    this.init();

    document.querySelector(".scrin-shot").addEventListener("click", () => {
      this.saveAsImage();
    });
  }

  init(): void {
    this.modelControllButtons();
    this.createLink();
    this.copyLink();
    this.closeLink();
    this.openRequest();
    this.closeRequest();
    this.openConfigWindowOnMobile();
    this.closeConfigWindowOnMobile();
    this.changeColorOfMaterial();
    this.closeHintInfo();
    this.showHintInfo();
    if (document.body.clientWidth <= 700) {
      this.viewer.camera.position.z = 220;
    }
  }

  //FIRST REQUEST
  getRequetForMainInitial(): void {
    const mainLoader: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".show-main-loader")
    );
    const colorsPalette: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".colors-palette")
    );

    const stringScripJSON: any =
      document.getElementsByTagName("script")[1].innerHTML; //change to [0]

    let readyScripJSON: any;

    for(let i = 0; i < document.getElementsByTagName("script").length; i++){
      const test: any = document.getElementsByTagName("script")[i].innerHTML;
      if(test.split("=")[0].includes('d3_app_data')){
        readyScripJSON = JSON.parse(
          test.split("=")[1].split(";")[0].split("'")[1]
        );
      }
    }

    // const readyScripJSON: any = JSON.parse(
    //   stringScripJSON.split("=")[1].split(";")[0].split("'")[1]
    // );

    const onlyUnique = (value: any, index: any, self: any) => {
      return self.indexOf(value) === index;
    };

    const unique = readyScripJSON.colors.filter(onlyUnique);
    for (let i = 0; i < unique.length; i++) {
      const div: HTMLDivElement = <HTMLDivElement>document.createElement("div");
      div.classList.add("color-palette");
      div.style.backgroundColor = `#${readyScripJSON.colors[i]}`;
      colorsPalette.append(div);
    }

    mainLoader.classList.add("hight-main-loader");
    const octagonWrap1: any = document.querySelector("#uv");

    axios.get(readyScripJSON.uvMap).then((res) => {
      octagonWrap1.innerHTML += res.data;
      const st1: any = document.getElementsByClassName("st1");
      for (let i = 0; i < st1.length; i++) {
        if (readyScripJSON.config) {
          st1[i].style.fill = readyScripJSON.config.activColor;
        } else {
          st1[i].style.fill = `#${readyScripJSON.colors[0]}`;
        }
      }
    });

    return readyScripJSON;
  }

  // Control buttons
  modelControllButtons(): void {
    let tempRotatValue: boolean = true;
    const rotate_btn = document.querySelector(".rotate_btn");

    // ROTATE MODEL
    rotate_btn.addEventListener("click", () => {
      this.viewer.controls.autoRotate = tempRotatValue;
      tempRotatValue ? (tempRotatValue = false) : (tempRotatValue = true);
    });

    // ZOOM IN MODEL
    const zoom_in_btn = document.querySelector(".zoom_in_btn");
    zoom_in_btn.addEventListener("click", () => {
      this.viewer.camera.position.z /= 1.1;
      this.viewer.camera.position.x /= 1.1;
      this.viewer.camera.position.y /= 1.1;
    });

    // ZOOM OUT MODEL
    const zoom_out_btn = document.querySelector(".zoom_out_btn");
    zoom_out_btn.addEventListener("click", () => {
      this.viewer.camera.position.z *= 1.1;
      this.viewer.camera.position.x *= 1.1;
      this.viewer.camera.position.y *= 1.1;
    });
  }

  //CHANGE COLOR OF MATERIAL
  changeColorOfMaterial(): void {
    let activeCcolor = "rgb(255, 255, 255)";
    let selectedColor;
    document
      .querySelector(".colors-palette")
      .addEventListener("click", (e: any) => {
        if (e.target.style.backgroundColor) {
          // e.target.classList.add('activColor');
          selectedColor = e.target.style.backgroundColor;

          const r = +e.target.style.backgroundColor
            ?.split(",")[0]
            .split("(")[1];
          const g = +e.target.style.backgroundColor?.split(",")[1];
          const b = +e.target.style.backgroundColor
            ?.split(",")[2]
            .split(")")[0];

          const hexColor =
            "0x" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16);

          this.viewer.scene.children.forEach((elem: any) => {
            if (elem.type === "Group") {
              elem.children.forEach((element: any) => {
                if (
                  element.name === "umbrella_fabric" ||
                  element.name === "umbrella_folds"
                ) {
                  if (hexColor === "0x1000000") {
                    for (
                      let i = 0;
                      i < this.viewer.scene.children.length;
                      i++
                    ) {
                      if (
                        this.viewer.scene.children[i].name ===
                        "directionalLight"
                      ) {
                        this.viewer.scene.children[i].position.set(
                          100,
                          120,
                          200
                        );
                        this.viewer.scene.children[i]["intensity"] = 0.6;
                        this.viewer.scene.children[i]["castShadow"] = true;
                      }
                    }
                    element.material.color.setHex("0x292929");
                  } else if (hexColor === "0x1ffffff") {
                    for (
                      let i = 0;
                      i < this.viewer.scene.children.length;
                      i++
                    ) {
                      if (
                        this.viewer.scene.children[i].name ===
                        "directionalLight"
                      ) {
                        this.viewer.scene.children[i].position.set(0, 100, 0);
                        this.viewer.scene.children[i]["intensity"] = 0.3;
                        this.viewer.scene.children[i]["castShadow"] = false;
                      }
                    }
                    element.material.color.setHex(hexColor);
                  } else {
                    for (
                      let i = 0;
                      i < this.viewer.scene.children.length;
                      i++
                    ) {
                      if (
                        this.viewer.scene.children[i].name ===
                        "directionalLight"
                      ) {
                        this.viewer.scene.children[i].position.set(
                          100,
                          120,
                          200
                        );
                        this.viewer.scene.children[i]["intensity"] = 0.6;
                        this.viewer.scene.children[i]["castShadow"] = true;
                      }
                    }
                    element.material.color.setHex(hexColor);
                  }
                }
              });
            }
          });

          if (activeCcolor !== selectedColor) {
            activeCcolor = selectedColor;

            const img: any = document.getElementsByClassName("st1");
            for (let i = 0; i < img.length; i++) {
              if (selectedColor === "rgb(255, 255, 255)") {
                selectedColor = "rgb(234, 234, 234)";
              }
              img[i].style.fill = selectedColor;
            }
          }
        }
      });
  }

  //SCRIN SHOT
  saveAsImage(): void {
    var imgData, imgNode;

    try {
      var strMime = "image/jpeg";
      imgData = this.viewer.renderer.domElement.toDataURL(strMime);

      this.saveFile(
        imgData.replace(strMime, "image/octet-stream"),
        "3D-product.jpg"
      );
    } catch (e) {
      console.log("screenshot erroe  --  ", e);
      return;
    }
  }

  saveFile(strData: any, filename: any): void {
    var link = document.createElement("a");
    if (typeof link.download === "string") {
      document.body.appendChild(link); //Firefox requires the link to be in the body
      link.download = filename;
      link.href = strData;
      link.click();
      document.body.removeChild(link); //remove the link when done
    }
  }

  //CREATE LINK
  createLink(): void {
    const createLinkBtn: HTMLButtonElement = <HTMLButtonElement>(
      document.querySelector(".create-link")
    );
    const popUp: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".pop-ap-window-link-info-container")
    );
    createLinkBtn.addEventListener("click", () => {
      popUp.classList.remove("hide-pop-ap-window-link-info-container");
    });
  }

  //COPY LINK
  copyLink(): void {
    const copyLinkBtn: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".link-copy-btn")
    );
    const copyLinkLine: any = document.querySelector(".link-line");
    copyLinkBtn.addEventListener("click", () => {
      copyLinkLine.select();
      copyLinkLine.setSelectionRange(0, 99999);
      document.execCommand("copy");
      copyLinkLine.style.backgroundColor = "rgb(91 251 10 / 42%)";
    });
  }

  //CLOSE LINK
  closeLink(): void {
    const closeLinkBtn: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".close-pop-ap-window-link-info")
    );
    const copyLinkLine: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".link-line")
    );
    closeLinkBtn.addEventListener("click", () => {
      copyLinkLine.style.backgroundColor = "white";
    });

    const popUp: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".pop-ap-window-link-info-container")
    );
    closeLinkBtn.addEventListener("click", () => {
      popUp.classList.add("hide-pop-ap-window-link-info-container");
    });
  }

  //OPEN USER  INFO POP UP
  openRequest(): void {
    const openRequestBtn: HTMLButtonElement = <HTMLButtonElement>(
      document.querySelector(".create-request")
    );
    const popUp: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".pop-ap-window-user-info-container")
    );
    openRequestBtn.addEventListener("click", () => {
      popUp.classList.remove("hide-pop-ap-window-user-info-container");
    });
  }

  //CLOSE USER  INFO POP UP
  closeRequest(): void {
    const closeLinkBtn: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".close-pop-ap-window-user-info")
    );
    const copyLinkLine: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".link-line")
    );

    const popUp: HTMLDivElement = <HTMLDivElement>(
      document.querySelector(".pop-ap-window-user-info-container")
    );
    closeLinkBtn.addEventListener("click", () => {
      popUp.classList.add("hide-pop-ap-window-user-info-container");
    });
  }

  openConfigWindowOnMobile(): void {
    const bearbeienBtn: HTMLButtonElement = <HTMLButtonElement>(
      document.getElementById("bearbeien")
    );
    const aside: any = <any>document.querySelector(".config-umbrella");
    bearbeienBtn.addEventListener("click", () => {
      aside.classList.add("pop-up-config");

        let c: HTMLDivElement = <HTMLDivElement>(
          document.querySelector(".fabric")
        );
        let cw = c.getBoundingClientRect();
        c.style.height = cw.width + "px";
        console.log(cw);
        console.log(cw.width);

    });
  }

  closeConfigWindowOnMobile(): void {
    const bearbeienBtn: HTMLButtonElement = <HTMLButtonElement>(
      document.getElementById("bearbeien-close")
    );
    const aside: any = <any>document.querySelector(".config-umbrella");
    bearbeienBtn.addEventListener("click", () =>
      aside.classList.remove("pop-up-config")
    );
  }

  //CLOSE HINT INFO
  closeHintInfo(): void {
    const closeIconButton = document.querySelector(".control-hint__close-icon");
    const info = document.querySelector(".control-hint");
    closeIconButton.addEventListener("click", () => {
      info.classList.add("control-hint__close");
    });
  }

  //SHOW HINT INFO
  showHintInfo(): void {
    const infoIconButton = document.querySelector(".control__info");
    const info = document.querySelector(".control-hint");
    infoIconButton.addEventListener("click", () => {
      info.classList.remove("control-hint__close");
    });
  }
}

export default App;
