import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  DirectionalLight,
  Mesh,
  HemisphereLight,
  Color,
  DirectionalLightHelper,
  AmbientLight
} from "three";
import * as THREE from 'three'
window.THREE = THREE;
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

declare global {
  interface Window {
    scene: any;
  }
}

export class Viewer {
  camera: PerspectiveCamera;
  scene: Scene;
  renderer: WebGLRenderer;
  controls: OrbitControls;
  hemisphereLight: HemisphereLight;
  container: any;
  directionalLight: DirectionalLight;
  directionalLightUnderUmbrella: DirectionalLight;
  texture : any;
  

  constructor() {
    this.init();
  }

  init(): void {
    // CONTAINER
    this.container = document.getElementById("threeJS");
    this.texture =  null;
    // CAMERA
    this.camera = new PerspectiveCamera(
      45,
      this.container.offsetWidth / this.container.offsetHeight,
      1,
      2000
    );
    this.camera.position.set(0, 60, 160);
    
    // SCENE
    this.scene = window.scene = new Scene();
    window.scene = this.scene;
    this.scene.background = new Color('#ffffff');

    this.scene.add(this.camera);

    // RENDERER
    this.renderer = new WebGLRenderer({ antialias: true, alpha: true, preserveDrawingBuffer: true });
    this.renderer.shadowMap.enabled = true;
    // this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    // this.renderer.setPixelRatio(window.devicePixelRatio);
    // this.renderer.setSize(
    //   this.container.offsetWidth*2,
    //   this.container.offsetHeight*2
    // );
    // this.renderer.shadowMap.enabled = true;
    


    // ADD ON SCENE
    this.container.appendChild(this.renderer.domElement);

    // LIGHT
    this.initLight();

    // CONTROLS
    this.initControls();

    // RESIZE EVENT
    window.addEventListener("resize", this.onWindowResize.bind(this));

    setTimeout(() => {
      this.onWindowResize();
    }, 0);

    this.animate();
  }

  initLight() {  
      const skyColor = 0xffffff; 
      const groundColor = 0xffffff;
      const intensityHemisphereLight = 0.70;
      this.hemisphereLight = new HemisphereLight(skyColor, groundColor, intensityHemisphereLight);
      this.hemisphereLight.name = "HemisphereLight";
      this.scene.add(this.hemisphereLight);  
    
      const color = 0xFFFFFF;
      const intensityDirectionalLight = 0.6;
      this.directionalLight = new DirectionalLight(color, intensityDirectionalLight);
      this.directionalLight.name = 'directionalLight';
      this.directionalLight.position.set(100, 120, 200);
      this.directionalLight.castShadow = true;

      this.directionalLight.shadow.mapSize.width = 1024;
      this.directionalLight.shadow.mapSize.height = 1024;

      this.directionalLight.shadow.camera.near = 0.1; // default
      this.directionalLight.shadow.camera.far = 500;     

      this.directionalLight.shadow.camera.left = -100; // default
      this.directionalLight.shadow.camera.right = 100; // default
      this.directionalLight.shadow.camera.top = 100; // default
      this.directionalLight.shadow.camera.bottom = -100; // default

      this.scene.add(this.directionalLight);
      this.scene.add(this.directionalLight.target);

      const light = new THREE.PointLight( 0xffffff, 0.7, 50 );
      light.position.set( 0, 0, 0 );
      this.scene.add( light );

  }

  initControls() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.maxDistance = 300;
    this.controls.minDistance = 10;
    // this.controls.enablePan = false;
    // this.controls.maxPolarAngle = 1.85;//Math.PI/2
  }

  onWindowResize() {
    this.camera.aspect =
      this.container.offsetWidth / this.container.offsetHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(
      this.container.offsetWidth,
      this.container.offsetHeight
    );
  }

  animate() {
    requestAnimationFrame(() => {
      this.animate();
    });
    if(this.texture){
      this.texture.anisotropy = 16;
      // this.texture.generateMipmaps = false;
      this.texture.minFilter = THREE.LinearFilter;
      this.texture.needsUpdate = true;
    }

    this.controls.update();
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.render(this.scene, this.camera);
    
  }
}

export default Viewer;
