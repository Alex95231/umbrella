import {
  MeshBasicMaterial,
  MeshStandardMaterial,
  Mesh,
  LoadingManager,
  Texture,
  ObjectLoader,
  CompressedTextureLoader,
  
} from "three";
import Viewer from "./Viewer";

// import modelJSON from '../../../../../grоup_5_m/group_3/9173-0001/9173-0001.obj';


export class Loader {
  material: MeshBasicMaterial;
  sphere: Mesh;
  loader: ObjectLoader;
  manager: LoadingManager;
  model: any;
  viewer: Viewer;
  activeColor: any;


  constructor(viewer: Viewer , model: any, config: any ) {
    // this.model = modelJSON//model;
    this.viewer = viewer;
    this.loader = new ObjectLoader();
    this.model = config.modelUrl;
    if(config.config){
      const tempStrArr = config.config.activColor.split('(')[1].split(')')[0].split(',');
      const tempNumArr = tempStrArr.map((i:string)=>{
        return +i;
      })
      this.rgbToHex(tempNumArr[0],tempNumArr[1],tempNumArr[2]) === '000000' ? this.activeColor = '222222' : this.activeColor = this.rgbToHex(tempNumArr[0],tempNumArr[1],tempNumArr[2]);
    }
    else{
      config.colors[0] === '000000' ? this.activeColor = '222222' : this.activeColor = config.colors[0];
    }
    
    this.loadModel();
  }

   componentToHex(c:any) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
  }
  
   rgbToHex(r: number, g: number, b: number) {
    return this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
  }

  loadModel(): void {
      this.loader.load(this.model , ( object ) => {
        object.traverse((item : any) => {

                if(item.name === "umbrella_fabric"){
                  item.material.side = 2;
                  item.material.color.setHex(`0x${this.activeColor}`);
                }
                if(item.name === "umbrella_fabric_label"){
                  const canvasCcontainer: HTMLCanvasElement = <HTMLCanvasElement> document.getElementById( "canvas" );  
                  this.viewer.texture = new Texture( canvasCcontainer );
                  item.material.map = this.viewer.texture;
                  item.material.transparent = true;
                }
        
                if(item.isMesh){
                  item.material.shininess = 0;
                  item.castShadow = true;
                }
              })
        object.position.y = -40;
        this.viewer.scene.add( object );
        const preloader = document.getElementById('threeJS-preloader');    
        preloader.classList.remove('threeJS-preloader');
        this.firstFullRotate();
      } );
  }

  // first full rotate
  firstFullRotate(): void {
      this.viewer.controls.maxAzimuthAngle = -0.023398320284549114;
      this.viewer.controls.minAzimuthAngle = 0.023398320284549114;
      this.viewer.controls.autoRotate = true;
      this.viewer.controls.autoRotateSpeed = 10;

      setTimeout(() => {
        this.viewer.controls.autoRotate = false;
        this.viewer.controls.minAzimuthAngle = -Infinity;
        this.viewer.controls.maxAzimuthAngle  = Infinity;
      }, 10000);
   

  }
}

export default Loader;
