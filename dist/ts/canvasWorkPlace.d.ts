export declare class CanvasWorkPlace {
    config: any;
    constructor(config: any);
    uploadFilesFromConfi(canvas: any, config: any): void;
    uploadFiles(canvas: any, id: string): void;
    addFileToFabricJS(canvas: any, file: any, id: string, config: any): void;
    uploadFilesToCanvas(canvas: any, file: any, id: string, config: any): void;
    copyFile(canvas: any, event: any): void;
    removeImageFromCanvas(canvas: any, event: any): void;
    onClickImage(canvas: any, id: string): void;
    onClickBody(canvas: any): void;
    removeBackgroundColor(canvas: any, event: any): void;
}
