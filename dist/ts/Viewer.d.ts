import { Scene, PerspectiveCamera, WebGLRenderer, DirectionalLight, HemisphereLight } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
declare global {
    interface Window {
        scene: any;
    }
}
export declare class Viewer {
    camera: PerspectiveCamera;
    scene: Scene;
    renderer: WebGLRenderer;
    controls: OrbitControls;
    hemisphereLight: HemisphereLight;
    container: any;
    directionalLight: DirectionalLight;
    directionalLightUnderUmbrella: DirectionalLight;
    texture: any;
    constructor();
    init(): void;
    initLight(): void;
    initControls(): void;
    onWindowResize(): void;
    animate(): void;
}
export default Viewer;
