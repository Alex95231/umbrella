import { MeshBasicMaterial, Mesh, LoadingManager, ObjectLoader } from "three";
import Viewer from "./Viewer";
export declare class Loader {
    material: MeshBasicMaterial;
    sphere: Mesh;
    loader: ObjectLoader;
    manager: LoadingManager;
    model: any;
    viewer: Viewer;
    activeColor: any;
    constructor(viewer: Viewer, model: any, config: any);
    componentToHex(c: any): any;
    rgbToHex(r: number, g: number, b: number): any;
    loadModel(): void;
    firstFullRotate(): void;
}
export default Loader;
