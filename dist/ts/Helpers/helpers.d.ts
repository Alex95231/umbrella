declare class Helpers {
    canvas: fabric.Canvas;
    constructor(canvas: fabric.Canvas);
    dataURItoBlob(dataURI: any): Blob;
    getAllObjects(type: string): void;
    changeLanguage(): void;
    changeAllTextToSelectedLanguage(language: any): void;
}
export default Helpers;
