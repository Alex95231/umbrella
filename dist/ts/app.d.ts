import { SphereGeometry, MeshBasicMaterial, Mesh } from "three";
import Viewer from "./Viewer";
import Loader from "./loader";
export declare class App {
    viewer: Viewer;
    geometry: SphereGeometry;
    material: MeshBasicMaterial;
    sphere: Mesh;
    loader: Loader;
    canvasWorkPlace: any;
    constructor();
    init(): void;
    getRequetForMainInitial(): void;
    modelControllButtons(): void;
    changeColorOfMaterial(): void;
    saveAsImage(): void;
    saveFile(strData: any, filename: any): void;
    createLink(): void;
    copyLink(): void;
    closeLink(): void;
    openRequest(): void;
    closeRequest(): void;
    openConfigWindowOnMobile(): void;
    closeConfigWindowOnMobile(): void;
    closeHintInfo(): void;
    showHintInfo(): void;
}
export default App;
