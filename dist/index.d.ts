import './styles/styles.css';
import './styles/reset.css';
import './styles/viewer.css';
import './styles/global.css';
import './styles/preloader.css';
import './styles/pop-up-link-info.css';
import "./styles/customSelect.css";
