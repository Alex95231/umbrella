<?php 
	include_once('../3d-design-data.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  
  <!-- schirmmacher.com Styles -->
  <link href="<?php echo $path_css; ?>" rel="stylesheet">
  
  <link rel="shortcut icon" href="#">
  <title>schirmmacher - 3D-Designer</title>
</head>
<body>

  <script>
	  
	// config data ---------------------------------
   d3_app_data = '<?php echo $data_array_string; ?>';
	
	
	// Text strings / translations -----------------
	d3_app_texts = '<?php echo $strings_array_string; ?>';
	// Replace unwanted characters
	d3_app_texts = d3_app_texts.replace(/[\u0000-\u0019]+/g,""); 
	// Save data in object
	d3_app_texts_object = JSON.parse(d3_app_texts);
	// Write text into document
	function writeText(stringToWrite){
		if(d3_app_texts_object.strings[stringToWrite] !== undefined  && d3_app_texts_object.strings[stringToWrite] !== null){
			document.write(d3_app_texts_object.strings[stringToWrite]);
		}else{
			document.write('<span style="display: inline-block; padding: 3px; background:red; color: #fff;">MISSING STRING: '+stringToWrite+'</span>');
		}
	}
	
  </script>
	
	
  <div class="show-main-loader">
    <div class="loader"></div>
  </div>
  
  <div class="main">
    <aside class="config-umbrella">
      <div>
      <div class="config-umbrella__header">
        <div class="config-umbrella__collors">
          <h4 id="colors" class="bolt">Farbe Wählen</h4>
          <div class="colors-palette">
          </div>
        </div>
        <div class="config-umbrella__files">
          <span class="add-file__button">
            <span class="add-file__input">
              <h5 id="addFile" class="add-file__text font-famil">+Motiv hochlladen</h5>
              <input id="image-file" type="file" name="files" multiple="true" />
            </span>
            <!-- <span class="add-file__info">i</span> -->
            <i class= "material-icons __info add-file__info">info_outline</i>
            <span class="add-file__hint">
              <div class="add-file__text-hint"><i class="material-icons add-file__icon-hint">star_border</i> - <div id="magicWand" class="font-famil">Motiv automatisch freistellen</div> </div>
              <div class="add-file__text-hint"><i class="material-icons add-file__icon-hint">clear</i> - <div id="delete" class="font-famil">Motiv löschen</div></div>
              <div class="add-file__text-hint"><i class="material-icons add-file__icon-hint">content_copy</i> -  <div id="copy" class="font-famil">Motiv duplizieren/kopieren</div></div>
            </span>
          </span>
          <div id="files-list">
            <ul id='ul-list-files'></ul>
          </div>
        </div>
      </div>
      <h4 id="designTemplate" class="bolt">Design-Vorlage</h4>
      <div class="fabric">
        <div  id="uv-preloader" class="uv-preloader-hide">
          <div class="loader"></div>
        </div>
        <div class='octagonWrap'>
        <div id="uv"></div>
          <!-- -->
          <div class='octagon'>
            <canvas id="canvas" width="300" height="300"></canvas>
          </div>


        </div>
      </div>
      <!--BUTTON FOR MOBILE VERSION -->
      <div class="btn-container bearbeien-btn-container">
        <button id="bearbeien-close" class="bearbeien bearbeien-btn font-famil">3D Model anzeigen</button>
      </div>

      
    </div>
    <!-- <h4 id="designNoteHeader" class="bolt">Gestaltungshinweis:</h4> -->
    <h4 id="designNoteP" class="font-famil">en können nur Siebdruck-Motive umgesetzt werden.</h4>
    </aside>
    <nav class="model">
      <div id="threeJS">
        <div  id="threeJS-preloader" class="threeJS-preloader">
          <div class="loader"></div>
        </div>
      </div>
      <div class="model__controls">
        <i class="material-icons control rotate_btn">3d_rotation</i>
        <i class="material-icons control zoom_out_btn">zoom_out</i>
        <i class= "material-icons control zoom_in_btn">zoom_in</i>
      </div>
      <i class= "material-icons control__info">info_outline</i>
      
      <div class="control-hint">
        <i class="large material-icons control-hint__close-icon">close</i>
        <!-- <h1 class="control-hint__close">X</h1> -->
        <div>
          <div class="control-hint__hint">
          <i class="material-icons control">mouse</i>
          <span id ="hold&drag" class="font-famil"> - Hold and drag</span>
          </div>
          <div class="control-hint__hint">
          <i class="material-icons control">mouse</i>
          <span id="wheel" class="font-famil"> - Wheel</span>
          </div>
        </div>
      </div>
    </nav>
  </div>

  <!--BUTTON FOR MOBILE VERSION -->
  <div class="btn-container " >
    <button id="bearbeien" class="bearbeien bearbeien-btn font-famil">Bearbeien</button>
  </div>


  <div class="btn-container">
    <button id="screenshot" class="scrin-shot footer-btn font-famil">Bild exportieren</button>
    <button id="createPath" class="create-link footer-btn font-famil">Entwurf teilen</button>
    <button id="createRequest" class="create-request footer-btn font-famil">Anfragen senden</button>

      <!--CUSTOM SELECT-->

    <!-- <div class="custom-select-wrapper">
      <div class="custom-select">
          <div class="custom-select__trigger"><span>Du</span>
              <div class="arrow"></div>
          </div>
          <div class="custom-options">
              <span class="custom-option selected font-famil font-famil" data-value="Du">Du</span>
              <span class="custom-option font-famil font-famil" data-value="En">En</span>
              <span class="custom-option font-famil font-famil" data-value="Uk">Uk</span>
          </div>
      </div>
  </div>
  </div> -->

  <!-- Pop up for 7 button -->

  <div class='pop-ap-window-link-info-container hide-pop-ap-window-link-info-container'>
    <div class="close-pop-ap-window-link-info">
      <i class="large material-icons color-icon-close">close</i>
    </div>
    <div class='pop-ap-window-link-info'>
      <div class="pop-ap-row">
        <!-- <div class='link-line'>{your_domain}/?id={id from server} </div> -->
        <div class="link-container">
          <input class='link-line' type="text" value="" readonly></input>
          <div class='link-line-preloader'></div>
    </div>
        <div class="link-copy-btn"><i class="large material-icons ">content_copy</i></div>
      </div>   
    </div>
  </div>

  <!-- Pop up for 8 button -->
  
    <div class='pop-ap-window-user-info-container hide-pop-ap-window-user-info-container'>
    <div class="close-pop-ap-window-user-info">
      <i class="large material-icons color-icon-close">close</i>
    </div>
    <div class='pop-ap-window-user-info'>
      <div>
        <p id="email" cclass="font-famil">E-mail</p>
        <input></input>
      </div>
      <div>
        <p id="firstName" cclass="font-famil">Name</p>
        <input></input>
      </div>
      <div>
        <p id="secondName" cclass="font-famil">Surname</p>
        <input></input>
      </div>
      <div>
        <p id="quantity" cclass="font-famil">Quantity</p>
        <input></input>
      </div>
      <div>
        <p id="comment" cclass="font-famil">Comment</p>
        <textarea cols="40" rows="5"></textarea>
      </div>
      <div>
        <p id="link" cclass="font-famil">Link</p>
        <input value='' readonly class="link-line-second"></input>
      </div>
      <div class="sent-request"><p id="sendRequest" cclass="font-famil">Send</p></div>
    </div>
  </div>
  </div>
  <div>
    <!-- <canvas id="second-canvas"  style="background-color: red;"></canvas> -->
  </div>
<script type="text/javascript" src="app.bundle.js"></script>

<!-- schirmmacher.com Javascript -->
<script>
	var schirmmacher_domain = '<?php echo $http_protocol.$current_domain; ?>';
</script>
<script src="<?php echo $path_js; ?>" type="text/javascript" ></script>


</body>
</html>